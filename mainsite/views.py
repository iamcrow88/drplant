from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post
from datetime import datetime
import pymysql

# Create your views here.
def showindex(request):
	now = datetime.now()

	# 打开数据库连接
	db = pymysql.connect("localhost","test123","test123","aiotdb" )
	
	# 使用 cursor() 方法创建一个游标对象 cursor
	cursor = db.cursor()
	 
	# 使用 execute()  方法执行 SQL 查询 
	cursor.execute("SELECT * FROM `sensors` ORDER BY `time` DESC LIMIT 15")
	 
	# 使用 fetchone() 方法获取单条数据.
	data = cursor.fetchall()
	
	time = [str(rows[1]) for rows in data]
	
	light = [rows[2] for rows in data]
	last_light = data[0][2]

	temp = [rows[3] for rows in data]
	last_temp = data[0][3]

	humid = [rows[4] for rows in data]
	last_humid = data[0][4]

	soil_moisture = [rows[5] for rows in data]
	last_soil_moisture = data[0][5]

	# 关闭数据库连接
	cursor.close()
	db.close()

	return render(request, 'index.html', locals())
