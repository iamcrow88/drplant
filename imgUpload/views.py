from django.shortcuts import render
from imgUpload.models import IMG

# Create your views here.
# 上傳圖片
def imgUpload(request):
    if request.method == 'POST':
        img = IMG(img_url=request.FILES.get('img'))
        img.save()
    return render(request, 'imgUpload.html')

# 顯示圖片
def imgShow(request):
    imgs = IMG.objects.all()
    context = {
        'imgs' : imgs
    }
    return render(request, 'imgShow.html', context)