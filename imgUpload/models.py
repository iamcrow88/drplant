from django.db import models

# Create your models here.
class IMG(models.Model):
    # upload_to為圖片上傳的路徑，不存在就創建一個新的。
    img_url = models.ImageField(upload_to='img')